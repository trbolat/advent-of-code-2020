package main

import (
    "fmt"
    "io/ioutil"
    "math"
    "os"
    "regexp"
    "strconv"
    "strings"
)

func loadInstructions(fileName string) []Instruction {
    rawInput, _ := ioutil.ReadFile(fileName)
    sanitizedInput := strings.TrimSuffix(string(rawInput), "\n")
    input := strings.Split(sanitizedInput, "\n")

    nameRegex := regexp.MustCompile("[a-zA-Z]")
    valueRegex := regexp.MustCompile("\\d+")
    instructions := make([]Instruction, 0)
    for i := 0; i < len(input); i++ {
        data := input[i]

        value, err := strconv.Atoi(valueRegex.FindString(data))
        if err != nil {
            fmt.Println(err)
            os.Exit(1)
        }

        instructions = append(instructions, Instruction{
            name:  nameRegex.FindString(data),
            value: value,
        })
    }
    return instructions
}

type Instruction struct {
    name  string
    value int
}

// 817 too low
func main() {
    // run("demo.txt")
    run("input.txt")
}

func run(fileName string) {
    instructions := loadInstructions(fileName)

    dirX := 1
    dirY := 0
    x := 0
    y := 0

    for _, instruction := range instructions {
        switch instruction.name {
        case "N":
            y += instruction.value
        case "S":
            y -= instruction.value
        case "E":
            x += instruction.value
        case "W":
            x -= instruction.value
        case "F":
            x += dirX * instruction.value
            y += dirY * instruction.value
        case "L":
            fallthrough
        case "R":
            dirX, dirY = getNewDirections(instruction, dirX, dirY)
        }
        fmt.Println("new state is:", instruction, x, y, dirX, dirY)
    }
    fmt.Println(x, "+", y, "=", math.Abs(float64(x))+math.Abs(float64(y)))
}

func getNewDirections(instruction Instruction, dirX int, dirY int) (int, int) {
    currentDegrees := (math.Atan2(float64(dirX), float64(dirY)) * 180) / math.Pi
    var degrees float64

    if instruction.name == "R" {
        degrees = currentDegrees + float64(instruction.value)
    } else {
        degrees = currentDegrees - float64(instruction.value)
    }

    radians := degrees * math.Pi / 180

    deltaX := int(math.Round(math.Sin(radians)))
    deltaY := int(math.Round(math.Cos(radians)))

    return deltaX, deltaY
}
