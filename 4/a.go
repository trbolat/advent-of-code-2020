package main

import (
    "fmt"
    "io/ioutil"
    "strings"
)

const passportPairSeparator = ":"

type requirement struct {
    key      string
    required bool
}

var passportRequirements []requirement

func init() {
    passportRequirements = []requirement{
        {key: "byr", required: true},
        {key: "iyr", required: true},
        {key: "eyr", required: true},
        {key: "hgt", required: true},
        {key: "hcl", required: true},
        {key: "ecl", required: true},
        {key: "pid", required: true},
        {key: "cid", required: false},
    }
}

func main() {
    rawInput, _ := ioutil.ReadFile("./input.txt")
    passports := strings.Split(string(rawInput), "\n\n")

    cnt := 0
    for _, passport := range passports {
        passport = sanitizePassport(passport)

        if isPassportValid(passport) {
            cnt++
        }
    }

    fmt.Println(cnt)
}

func isPassportValid(passport string) bool {
    for _, req := range passportRequirements {
        searchFor := req.key + passportPairSeparator
        requirementFulfilled := strings.Contains(passport, searchFor)

        if !requirementFulfilled && req.required {
            return false
        }
    }
    return true
}

func sanitizePassport(passport string) string {
    return strings.Replace(passport, "\n", "", -1)
}
