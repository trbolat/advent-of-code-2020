package main

import (
    "fmt"
    "io/ioutil"
    "regexp"
    "strconv"
    "strings"
)

const passportPairSeparator = ":"

type isRequirementFulfilled func(string) bool

type requirement struct {
    key              string
    required         bool
    checkRequirement isRequirementFulfilled
}

var passportRequirements []requirement

func init() {
    passportRequirements = []requirement{
        {
            key:      "byr",
            required: true,
            checkRequirement: func(s string) bool {
                i, err := strconv.Atoi(s)
                if err != nil {
                    return false
                }
                return 1920 <= i && i <= 2002
            },
        },
        {
            key:      "iyr",
            required: true,
            checkRequirement: func(s string) bool {
                i, err := strconv.Atoi(s)
                if err != nil {
                    return false
                }
                return 2010 <= i && i <= 2020
            },
        },
        {
            key:      "eyr",
            required: true,
            checkRequirement: func(s string) bool {
                i, err := strconv.Atoi(s)
                if err != nil {
                    return false
                }
                return 2020 <= i && i <= 2030
            },
        },
        {
            key:      "hgt",
            required: true,
            checkRequirement: func(s string) bool {
                cmRegex := regexp.MustCompile("^\\d+cm$")
                if cmRegex.MatchString(s) {
                    height, err := strconv.Atoi(strings.Replace(s, "cm", "", 1))
                    if err != nil {
                        return false
                    }
                    return 150 <= height && height <= 193
                }
                inRegex := regexp.MustCompile("^\\d+in$")
                if inRegex.MatchString(s) {
                    height, err := strconv.Atoi(strings.Replace(s, "in", "", 1))
                    if err != nil {
                        return false
                    }
                    return 59 <= height && height <= 76
                }
                return false
            },
        },
        {
            key:      "hcl",
            required: true,
            checkRequirement: func(s string) bool {
                regex := regexp.MustCompile("^#[0-9a-f]{6}$")
                return regex.MatchString(s)
            },
        },
        {
            key:      "ecl",
            required: true,
            checkRequirement: func(s string) bool {
                regex := regexp.MustCompile("^(amb|blu|brn|gry|grn|hzl|oth)$")
                return regex.MatchString(s)
            },
        },
        {
            key:      "pid",
            required: true,
            checkRequirement: func(s string) bool {
                regex := regexp.MustCompile("^[0-9]{9}$")
                return regex.MatchString(s)
            },
        },
        {
            key:      "cid",
            required: false,
            checkRequirement: func(s string) bool {
                return true
            },
        },
    }
}

func main() {
    rawInput, _ := ioutil.ReadFile("./input.txt")
    passports := strings.Split(string(rawInput), "\n\n")

    cnt := 0
    for _, passport := range passports {
        passport = sanitizePassport(passport)

        if isPassportValid(passport) {
            cnt++
        }
    }

    fmt.Println(cnt)
}

func isPassportValid(passport string) bool {
    for _, req := range passportRequirements {
        pairRegex := regexp.MustCompile(req.key + passportPairSeparator + "[^\\s]*")
        pair := pairRegex.FindString(passport)
        pairValue := strings.Replace(pair, req.key+passportPairSeparator, "", 1)

        if (pair == "" && req.required) || !req.checkRequirement(pairValue) {
            return false
        }
    }
    return true
}

func sanitizePassport(passport string) string {
    return strings.Replace(passport, "\n", " ", -1)
}
