package main

import (
    "fmt"
    "io/ioutil"
    "sort"
    "strconv"
    "strings"
)

const maximumDiff = 3

type Adapter struct {
    value   int
    enabled bool
}

func getAdapters(filename string) map[int]*Adapter {
    rawInput, _ := ioutil.ReadFile(filename)
    sanitizedInput := strings.TrimSuffix(string(rawInput), "\n")
    input := strings.Split(sanitizedInput, "\n")

    planeAdapter := 0
    adapterValues := make([]int, len(input))
    adapterValues = append(adapterValues, planeAdapter)
    for i, s := range input {
        adapterValues[i], _ = strconv.Atoi(s)
    }
    sort.Ints(adapterValues)

    targetAdapter := adapterValues[len(adapterValues)-1] + 3
    adapterValues = append(adapterValues, targetAdapter)

    adapters := make(map[int]*Adapter)
    cnt := len(adapterValues)

    for i := 0; i < len(adapterValues); i++ {
        adapterValue := adapterValues[i]
        adapters[cnt-i-1] = &Adapter{adapterValue, true}
    }
    return adapters
}

type IndexGroup struct {
    min int
    max int
}

func main() {
    adapters := getAdapters("input.txt")

    indexGroups := make([]IndexGroup, 0)
    g := IndexGroup{min: 0}
    for i := 0; i < len(adapters)-1; i++ {
        adapter := adapters[i]
        if adapter.value-adapters[i+1].value == maximumDiff {
            g.max = i
            indexGroups = append(indexGroups, g)
            g = IndexGroup{min: i + 1}
        }
    }
    g.max = len(adapters) - 1
    indexGroups = append(indexGroups, g)

    total := 1
    for _, group := range indexGroups {
        total *= permute(group.min, group.max, adapters)
    }
    fmt.Println(total)
}

func permute(startFromIndex int, stopAtIndex int, adapters map[int]*Adapter) int {
    cnt := 1
    for i := startFromIndex; i < stopAtIndex; i++ {
        adapter := adapters[i]
        if !adapter.enabled {
            continue
        }

        adapter.enabled = false
        isValid := isValidSequence(adapters)
        if isValid {
            cnt += permute(i+1, stopAtIndex, adapters)
        }
        adapter.enabled = true
    }
    return cnt
}

func isValidSequence(adapters map[int]*Adapter) bool {
    currentValue := adapters[0].value // airplane adapter value
    for i := 1; i < len(adapters); i++ {
        adapter := adapters[i]
        if !adapter.enabled {
            continue
        }
        if currentValue-adapter.value > maximumDiff {
            return false
        }
        currentValue = adapter.value
    }
    return true
}
