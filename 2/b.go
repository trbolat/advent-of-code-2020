package main

import (
    "fmt"
    "io/ioutil"
    "regexp"
    "strconv"
    "strings"
)

func main() {
    rawInput, _ := ioutil.ReadFile("./input.txt")
    items := strings.Split(string(rawInput), "\n")

    cnt := 0
    for _, item := range items {
        if isValid(item) {
            cnt++
        }
    }
    fmt.Println(cnt)
}

func isValid(policy string) bool {
    if policy == "" {
        return false
    }

    policy = strings.Replace(policy, "-", " ", -1)
    policy = strings.Replace(policy, ":", "", -1)

    regex := regexp.MustCompile(" ")
    policyItems := regex.Split(policy, -1)

    firstIndex, _ := strconv.Atoi(policyItems[0])
    secondIndex, _ := strconv.Atoi(policyItems[1])
    targetLetter := policyItems[2]
    subject := policyItems[3]

    first := string(subject[firstIndex-1])
    second := string(subject[secondIndex-1])

    return (first != second) && (first == targetLetter || second == targetLetter)
}
