package main

import (
    "fmt"
    "io/ioutil"
    "math"
    "os"
    "regexp"
    "strconv"
    "strings"
)

func loadInstructions(fileName string) []Instruction {
    rawInput, _ := ioutil.ReadFile(fileName)
    sanitizedInput := strings.TrimSuffix(string(rawInput), "\n")
    input := strings.Split(sanitizedInput, "\n")

    nameRegex := regexp.MustCompile("[a-zA-Z]")
    valueRegex := regexp.MustCompile("\\d+")
    instructions := make([]Instruction, 0)
    for i := 0; i < len(input); i++ {
        data := input[i]

        value, err := strconv.Atoi(valueRegex.FindString(data))
        if err != nil {
            fmt.Println(err)
            os.Exit(1)
        }

        instructions = append(instructions, Instruction{
            name:  nameRegex.FindString(data),
            value: value,
        })
    }
    return instructions
}

type Instruction struct {
    name  string
    value int
}

type WayPoint struct {
    x int
    y int
}

type Ship struct {
    x int
    y int
}

func main() {
    run("demo.txt")
    run("input.txt")
}

func run(fileName string) {
    instructions := loadInstructions(fileName)

    waypoint := WayPoint{10, 1}
    ship := Ship{0, 0}

    for _, instruction := range instructions {
        switch instruction.name {
        case "N":
            waypoint.y += instruction.value
        case "S":
            waypoint.y -= instruction.value
        case "E":
            waypoint.x += instruction.value
        case "W":
            waypoint.x -= instruction.value
        case "F":
            ship.x += waypoint.x * instruction.value
            ship.y += waypoint.y * instruction.value
        case "L":
            waypoint = waypoint.rotate(-instruction.value)
        case "R":
            waypoint = waypoint.rotate(instruction.value)
        }
    }
    fmt.Println(waypoint.x, "+", waypoint.y, "=", ship.getDistanceFromStart())
}

func (s Ship) getDistanceFromStart() int {
    return int(math.Abs(float64(s.x)) + math.Abs(float64(s.y)))
}

func (w WayPoint) rotate(degrees int) WayPoint {
    radians := float64(degrees) * math.Pi / 180
    oldX := w.x
    w.x = int(math.Round(float64(w.x)*math.Cos(radians) + float64(w.y)*math.Sin(radians)))
    w.y = int(math.Round(float64(w.y)*math.Cos(radians) - float64(oldX)*math.Sin(radians)))

    return w
}
