package main

import (
    "fmt"
    "io/ioutil"
    "strconv"
    "strings"
)

const TARGET_NUMBER = 2020

func main() {
    rawInput, err := ioutil.ReadFile("./input.txt")
    checkError(err)
    numbers := strings.Split(string(rawInput), "\n")

    for firstIndex, first := range numbers {
        for secondIndex, second := range numbers[firstIndex:] {
            for _, third := range numbers[secondIndex:] {
                first, err := strconv.Atoi(first)
                checkError(err)
                second, err := strconv.Atoi(second)
                checkError(err)
                third, err := strconv.Atoi(third)
                checkError(err)

                if (first + second + third) == TARGET_NUMBER {
                    fmt.Println(first * second * third)
                    return
                }
            }
        }
    }
}

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}
