package main

import (
    "fmt"
    "io/ioutil"
    "strconv"
    "strings"
)

const (
    EMPTY    = "L"
    FLOOR    = "."
    OCCUPIED = "#"
)

func loadMap(fileName string) map[int]map[int]string {
    rawInput, _ := ioutil.ReadFile(fileName)
    parsedInput := strings.Split(string(rawInput), "\n")

    seatMap := make(map[int]map[int]string)

    for rowIndex, row := range parsedInput {
        for columnIndex, seat := range row {
            _, rowExists := seatMap[rowIndex]
            if !rowExists {
                seatMap[rowIndex] = make(map[int]string)
            }
            seatMap[rowIndex][columnIndex] = string(seat)
        }
    }
    return seatMap
}

var vectors map[int][]int

func main() {
    vectors = map[int][]int{
        -1: {-1, 0, 1},
        0:  {-1, 1},
        1:  {-1, 0, 1},
    }

    run("demo.txt")
    run("input.txt")
}

func run(fileName string) {
    fmt.Println("=== Results for " + fileName + " ===")
    seats := loadMap(fileName)
    updatedSeats := runRound(seats)

    for !isMapEqual(seats, updatedSeats) {
        seats = updatedSeats
        updatedSeats = runRound(updatedSeats)
    }
    fmt.Println("Occupied seats: " + strconv.Itoa(getOccupiedSeatsTotal(updatedSeats)))
}

func runRound(oldMap map[int]map[int]string) map[int]map[int]string {
    nextMap := copySeatMap(oldMap)

    for rowIndex, row := range oldMap {
        for columnIndex, seat := range row {
            cnt := getCountOfOccupiedNearbySeats(rowIndex, columnIndex, oldMap)

            if seat == EMPTY && cnt == 0 {
                nextMap[rowIndex][columnIndex] = OCCUPIED
                continue
            }
            if seat == OCCUPIED && cnt >= 5 {
                nextMap[rowIndex][columnIndex] = EMPTY
                continue
            }
        }
    }
    return nextMap
}

func isMapEqual(map1 map[int]map[int]string, map2 map[int]map[int]string) bool {
    for i, m := range map1 {
        for i2, _ := range m {
            if map1[i][i2] != map2[i][i2] {
                return false
            }
        }
    }
    return true
}

func getOccupiedSeatsTotal(seatMap map[int]map[int]string) int {
    cnt := 0
    for _, row := range seatMap {
        for _, seat := range row {
            if seat == OCCUPIED {
                cnt++
            }
        }
    }
    return cnt
}

func getCountOfOccupiedNearbySeats(row int, column int, seatMap map[int]map[int]string) int {
    cnt := 0

    distance := 1
    for xVector, yVectors := range vectors {
        for _, yVector := range yVectors {
            for true {
                seat, seatExists := seatMap[row+distance*xVector][column+distance*yVector]
                if !seatExists {
                    break
                }
                if seat == OCCUPIED {
                    cnt++
                    break
                }
                if seat == EMPTY {
                    break
                }
                distance++
            }
            distance = 1
        }
    }
    return cnt
}

func copySeatMap(oldMap map[int]map[int]string) map[int]map[int]string {
    nextMap := make(map[int]map[int]string, len(oldMap))
    for rowIndex, row := range oldMap {
        for columnIndex, _ := range row {
            _, rowExists := nextMap[rowIndex]
            if !rowExists {
                nextMap[rowIndex] = make(map[int]string)
            }
            nextMap[rowIndex][columnIndex] = oldMap[rowIndex][columnIndex]
        }
    }
    return nextMap
}

func printSeats(seatMap map[int]map[int]string) {
    output := ""
    for x := 0; x < len(seatMap); x++ {
        for y := 0; y < len(seatMap[x]); y++ {
            output += seatMap[x][y]
            if y < len(seatMap[x])-1 {
                output += " "
            }
        }
        output += "\n"
    }
    fmt.Println(output)
}
