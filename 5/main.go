package main

import (
    "errors"
    "fmt"
    "io/ioutil"
    "math"
    "strconv"
    "strings"
)

var seatMap = make(map[int]map[int]int)

func main() {
    rawInput, _ := ioutil.ReadFile("./input.txt")
    boardingPasses := strings.Split(string(rawInput), "\n")

    maxSeatId := 0

    for _, pass := range boardingPasses {
        if pass == "" {
            continue
        }
        x := decodeRow(pass)
        y := decodeColumn(pass)
        if seatMap[x] == nil {
            seatMap[x] = make(map[int]int)
        }
        seatMap[x][y] = 1

        seatId := x*8 + y
        if seatId > maxSeatId {
            maxSeatId = seatId
        }
    }
    fmt.Println("Max seat id is " + strconv.Itoa(maxSeatId))
    mySeatId, err := findMySeat()
    checkError(err)
    fmt.Println("My seat id is " + strconv.Itoa(mySeatId))
}

func findMySeat() (int, error) {
    countOfEmptyRowsAtStart := 4

    for x, m := range seatMap {
        for y := 0; y < 8; y++ {
            if x == len(seatMap)+countOfEmptyRowsAtStart {
                continue // we have guaranteed that we do not sit here
            }
            if m[y] == 0 {
                return x*8 + y, nil
            }
        }
    }
    return 0, errors.New("my seat was not found :(")
}

func decodeRow(boardingPass string) int {
    encodedColumn := boardingPass[0:7]
    return decodeBy(encodedColumn, "B")
}

func decodeColumn(boardingPass string) int {
    encodedRow := boardingPass[7:10]
    return decodeBy(encodedRow, "R")
}

func decodeBy(encoded string, splitBy string) int {
    stringLength := len(encoded)
    sum := 0
    for i := 0; i < stringLength; i++ {
        if encoded[i:i+1] == splitBy {
            sum += powInt(2, stringLength-i-1)
        }
    }
    return sum
}

func powInt(x, y int) int {
    return int(math.Pow(float64(x), float64(y)))
}

func checkError(e error) {
    if e != nil {
        panic(e)
    }
}
